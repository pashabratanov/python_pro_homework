from faker import Faker
from flask import Flask, render_template
from requests import get

app = Flask(__name__)


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/requirements')
def requirements():
    lst: list = []

    with open('requirements.txt') as f:
        lst = [*f.readlines()]

    context = {'requirements': lst}
    return render_template('requirements.html', **context)


@app.route('/generate-users')
def generate_users():
    lst: list = []

    fake = Faker()

    for i in range(1, 101):
        lst.append({'number': i, 'name': fake.first_name(), 'email': fake.email()})

    context: dict = {'users': lst}
    return render_template('generate_users.html', **context)


@app.route('/space')
def space():
    count_of_astronauts = 0

    try:
        r = get('http://api.open-notify.org/astros.json')
    except Exception as e:
        print(e)
    else:
        if 300 > r.status_code >= 200:
            count_of_astronauts = r.json().get('number')

    context: dict = {'number': count_of_astronauts}
    return render_template('space.html', **context)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5001, debug=True)
