from django import forms
from core.models import UniversityGroup, Student, Teacher, Lesson, HomeTask
from django.contrib.auth import get_user_model, authenticate, login


class StudentForm(forms.ModelForm):
    def __init__(self, **kwargs):
        self.id = kwargs.pop('id', None)
        super().__init__(**kwargs)

    class Meta:
        model = Student
        fields = ['first_name', 'last_name', 'age', 'university_group']

    # check if the last_name is unique
    def clean(self):
        cleaned_data = super().clean()
        last_name = cleaned_data.get('last_name')
        count = get_user_model().objects.all().filter(last_name=last_name).count()
        if count > 0:
            if self.id is None or count != 1:
                raise forms.ValidationError('Last name must be unique')

    def save(self, commit=True):
        student = super().save(commit=False)
        student.username = f'{self.cleaned_data["first_name"]}{self.cleaned_data["last_name"]}'.lower()
        student.save()
        return student


class TeacherForm(forms.ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'
        widgets = {
            'groups': forms.widgets.CheckboxSelectMultiple
        }


class SignupForm(forms.ModelForm):
    password = forms.CharField(widget=forms.widgets.PasswordInput)
    check_password = forms.CharField(widget=forms.widgets.PasswordInput)

    class Meta:
        model = get_user_model()
        fields = ['first_name', 'last_name', 'username', 'email']

    def clean_check_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['check_password']:
            raise forms.ValidationError('Passwords do not match')
        return self.cleaned_data['check_password']

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['check_password'])
        user.save()
        return user


class SigninForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.widgets.PasswordInput())

    def clean(self):
        user = get_user_model().objects.filter(username=self.cleaned_data['username']).first()

        if not user or not user.check_password(self.cleaned_data['password']):
            raise forms.ValidationError('Incorrect username/password')

    def auth(self, request):
        user = authenticate(request, **self.cleaned_data)
        login(request, user)
        return user


class LessonForm(forms.ModelForm):
    def __init__(self, **kwargs):
        self.course_id = kwargs.pop('course_id', None)
        super().__init__(**kwargs)

    class Meta:
        fields = ['title', 'material']
        model = Lesson

    def save(self, commit=True):
        lesson = super().save(commit=False)
        lesson.course_id = self.course_id
        lesson.save()


class HomeTaskForm(forms.ModelForm):
    def __init__(self, **kwargs):
        self.lesson_id = kwargs.pop('lesson_id', None)
        super().__init__(**kwargs)

    class Meta:
        fields = ['todo']
        model = HomeTask

    def save(self, commit=True):
        hometask = super().save(commit=False)
        hometask.lesson_id = self.lesson_id
        hometask.save()
