from django.contrib.auth.mixins import PermissionRequiredMixin
from django.shortcuts import redirect, get_object_or_404
from django.views.generic import TemplateView
from django.views.generic.edit import FormView, CreateView
from django.views.generic.detail import DetailView
from core.models import Student, Teacher, UniversityGroup, Course, Lesson, HomeTask
from core.forms import StudentForm, TeacherForm, SignupForm, SigninForm, LessonForm, HomeTaskForm
from django.contrib.auth import get_user_model
from faker import Faker
from random import choice


def create_random_students(count=1):
    faker = Faker()
    students: list = []

    groups = UniversityGroup.objects.all()
    if groups:
        for _ in range(count):
            first_name = faker.first_name()
            last_name = faker.last_name()
            random_attributes = {
                'username': first_name + last_name,
                'first_name': first_name,
                'last_name': last_name,
                'age': choice(range(18, 100)),
                'email': f'{first_name}@{last_name}.com'.lower(),
                'university_group': choice(groups)
            }
            students.append(Student.objects.create(**random_attributes))
        return students
    return []


class HomePageView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomePageView, self).get_context_data(**kwargs)
        context.update({'students': get_user_model().objects.all().order_by('-date_joined')[:10]})
        context.update({'some_text': 'my name is vova ia cook'})
        return context


class GenerateStudentView(PermissionRequiredMixin, TemplateView):
    template_name = 'generate_random_student.html'
    permission_required = 'university.can_add_students'

    def post(self, request):
        students = create_random_students()
        if students:
            return self.render_to_response({'students': students})
        return self.render_to_response({})


class GenerateLotsStudentsView(PermissionRequiredMixin, TemplateView):
    template_name = 'generate_lots_students.html'
    permission_required = 'core.can_add_students'

    def get_context_data(self, **kwargs):
        context = super(GenerateLotsStudentsView, self).get_context_data(**kwargs)
        count = self.request.GET.get('count', None)

        if not count or not count.isdigit() or not int(count) in range(1, 101):
            return context

        students: list = []

        students = create_random_students(count=int(count))
        if students:
            context.update({'students': students, 'generated_count': count})
        return context


class GroupsListView(TemplateView):
    template_name = 'groups.html'

    def get_context_data(self, **kwargs):
        context = super(GroupsListView, self).get_context_data(**kwargs)
        groups = UniversityGroup.objects.all()
        context.update({'groups': groups})
        return context


class TeachersListView(TemplateView):
    template_name = 'teachers.html'

    def get_context_data(self, **kwargs):
        context = super(TeachersListView, self).get_context_data(**kwargs)
        context.update({'teachers': Teacher.objects.all()})
        return context


class CreateStudentView(PermissionRequiredMixin, TemplateView):
    template_name = 'student_create.html'
    permission_required = 'core.can_add_students'

    def get_context_data(self, **kwargs):
        context = super(CreateStudentView, self).get_context_data(**kwargs)
        context['form'] = StudentForm()
        return context

    def post(self, request):
        form = StudentForm(data=request.POST)
        if form.is_valid():
            form.save()
            return redirect('/')
        return self.render_to_response(context={'form': form})


class CreateTeacherView(PermissionRequiredMixin, FormView):
    template_name = 'teacher_create.html'
    model = Teacher
    form_class = TeacherForm
    success_url = '/teachers/'
    permission_required = 'core.can_add_teacher'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class UpdateStudentView(PermissionRequiredMixin, TemplateView):
    template_name = 'student_create.html'
    permission_required = 'core.can_edit_students'

    def dispatch(self, request, *args, **kwargs):
        self.instance = get_object_or_404(Student, id=kwargs['id'])
        return super(UpdateStudentView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = StudentForm(
            instance=self.instance
        )
        return context

    def post(self, request, id):
        form = StudentForm(data=request.POST, instance=self.instance, id=id)

        if form.is_valid():
            form.save()
            return redirect('/')
        return self.render_to_response(context={'form': form})


class UpdateTeacherView(PermissionRequiredMixin, FormView):
    template_name = 'teacher_create.html'
    model = Teacher
    form_class = TeacherForm
    success_url = '/teachers/'
    permission_required = 'core.can_edit_teacher'

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        form_kwargs['instance'] = get_object_or_404(Teacher, id=self.kwargs['id'])
        return form_kwargs

    def form_valid(self, form):
        if form.is_valid():
            form.save()
        return super().form_valid(form)


class CoursesListView(TemplateView):
    template_name = 'courses.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({'courses': Course.objects.all()})
        return context


class SignupView(FormView):
    template_name = 'signup.html'

    form_class = SignupForm

    success_url = '/signin/'

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class SigninView(FormView):
    template_name = 'signin.html'

    form_class = SigninForm

    success_url = '/'

    def form_valid(self, form):
        form.auth(self.request)
        return super().form_valid(form)


class ProfileView(DetailView):
    model = Student
    template_name = 'profile.html'
    pk_url_kwarg = 'id'


class CourseView(DetailView):
    model = Course
    template_name = 'course.html'
    pk_url_kwarg = 'id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        course_id = self.kwargs.get('id', None)

        context.update({'lessons': get_object_or_404(Course, id=course_id).lessons.all()})
        return context

    def post(self, request, id):
        current_course = get_object_or_404(Course, id=id)

        if current_course in request.user.courses.all():
            request.user.courses.remove(current_course)
        else:
            if request.user.courses.filter(active=True).count() < 5:
                request.user.courses.add(current_course)
            else:
                return self.render_to_response({'error': 'You can not add 6th active course!', 'object': current_course})

        request.user.save()
        return redirect(request.path)


class LessonView(DetailView):
    template_name = 'lesson.html'
    model = Lesson
    pk_url_kwarg = 'id'


class CreateCourseView(PermissionRequiredMixin, CreateView):
    template_name = 'course_create.html'
    model = Course
    fields = '__all__'
    success_url = '/courses'
    permission_required = 'core.can_add_course'


class CreateLessonView(PermissionRequiredMixin, FormView):
    template_name = 'lesson_create.html'
    form_class = LessonForm
    permission_required = 'core.can_add_lesson'

    def dispatch(self, request, *args, **kwargs):
        self.course_id = kwargs.get('course_id', None)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = LessonForm(data=request.POST, course_id=self.course_id)
        if form.is_valid():
            form.save()
            return redirect(f'/courses/{self.course_id}')
        return self.render_to_response(context={'form': form})


class CreateHomeTaskView(PermissionRequiredMixin, FormView):
    template_name = 'hometask_create.html'
    form_class = HomeTaskForm
    permission_required = 'core.can_add_hometask'

    def dispatch(self, request, *args, **kwargs):
        self.lesson_id = kwargs.get('lesson_id', None)
        return super().dispatch(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        form = HomeTaskForm(data=request.POST, lesson_id=self.lesson_id)
        if form.is_valid():
            form.save()
            return redirect(f'/lessons/{self.lesson_id}')
        return self.render_to_response(context={'form': form})
