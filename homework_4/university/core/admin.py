from django.contrib import admin
from core.models import Student, ADDITIONAL_MODELS
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import gettext_lazy as _


class StudentProfileAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (_("Personal info"), {"fields": ("first_name", "last_name", "age", "email")}),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (_("University info"), {"fields": ("university_group", "courses")}),
        (_("Important dates"), {"fields": ("last_login", "date_joined")}),
    )


admin.site.register(Student, StudentProfileAdmin)

for model in ADDITIONAL_MODELS:
    admin.site.register(model)
