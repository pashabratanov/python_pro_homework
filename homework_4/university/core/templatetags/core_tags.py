from django import template
from django.template.defaultfilters import stringfilter

from core.models import Teacher

register = template.Library()


@register.filter
def get_even_numbers(lst):
    if type(lst) == list:
        return [elem for elem in lst if ((type(elem) == int) and (elem % 2 == 0))]


@register.filter
@stringfilter
def get_count_of_words(text):
    return text.count(' ') + 1


@register.inclusion_tag('includes/random_teachers_section.html')
def random_teachers(count=1):
    teachers = Teacher.objects.all().order_by('?')[:count]

    return {'teachers': teachers}
