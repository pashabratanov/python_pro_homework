from django.db import models
from django.contrib.auth.models import AbstractUser
from django.dispatch import receiver
from django.db.models.signals import post_save
from core.tasks import send_lesson_notification_to_subscribers, send_hometask_notification_to_subscribers


class TimeIt(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Organization(TimeIt):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class NameIt(TimeIt):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    class Meta:
        abstract = True


class Teacher(NameIt):
    subject = models.CharField(max_length=255)
    university_groups = models.ManyToManyField('core.UniversityGroup', blank=True)

    class Meta:
        permissions = (
            ('can_edit_teacher', 'Is user can edit teacher'),
            ('can_add_teacher', 'Is user can add teacher')
        )


class Student(AbstractUser):
    age = models.IntegerField(default=18)
    university_group = models.ForeignKey('core.UniversityGroup', on_delete=models.CASCADE, default=1)
    courses = models.ManyToManyField('core.Course', related_name='students')

    class Meta:
        permissions = (
            ('can_edit_students', 'Is user can edit students'),
            ('can_add_students', 'Is user can add students'),
        )


class UniversityGroup(Organization):
    curator = models.CharField(max_length=255)
    teachers = models.ManyToManyField(Teacher, blank=True)


class Course(Organization):
    active = models.BooleanField(default=False)

    class Meta:
        permissions = (
            ('can_add_course', 'Is user can add course'),
        )


class Lesson(models.Model):
    title = models.CharField(max_length=255)
    material = models.CharField(max_length=255)
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='lessons')

    def __str__(self):
        return self.title

    permissions = (
        ('can_add_lesson', 'Is user can add lesson'),
    )


class HomeTask(models.Model):
    todo = models.CharField(max_length=255)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE, related_name='home_tasks')

    def __str__(self):
        return self.todo

    permissions = (
        ('can_add_hometask', 'Is user can add hometask'),
    )


ADDITIONAL_MODELS = [Teacher, UniversityGroup, Course, Lesson, HomeTask]


@receiver(post_save, sender=Lesson)
def lesson_create_handler(instance, created, **kwargs):
    if created:
        send_lesson_notification_to_subscribers.delay(instance.id)


@receiver(post_save, sender=HomeTask)
def hometask_create_handler(instance, created, **kwargs):
    if created:
        send_hometask_notification_to_subscribers.delay(instance.id)

