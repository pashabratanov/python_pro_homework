from django.core.mail import send_mail
from university.celery import app
from django.contrib.auth import get_user_model
import datetime
from django.utils import timezone
import time


# add admin email as sender to send_mail
def send_email_as_admin(title, body, email):
    send_mail(title, body, 'admin@example.com', [email])


@app.task
def send_lesson_notification_to_subscribers(lesson_id):
    from core.models import Lesson
    time.sleep(1) # to resolve a notification bug when the lesson is created via admin panel
    lesson = Lesson.objects.get(id=lesson_id)
    text = f'A new lesson {lesson} of {lesson.course} has been recently added.'

    for subscriber in lesson.course.students.all():
        send_email_as_admin('New lesson', text, subscriber.email)


@app.task
def send_hometask_notification_to_subscribers(hometask_id):
    from core.models import HomeTask
    time.sleep(1)  # to resolve a notification bug when the hometask is created via admin panel
    hometask = HomeTask.objects.get(id=hometask_id)
    text = f'A new hometask: {hometask} of in {hometask.lesson} in {hometask.lesson.course} course, has been recently added.'

    for subscriber in hometask.lesson.course.students.all():
        send_email_as_admin('New hometask', text, subscriber.email)


@app.task
def about_new_courses():
    from core.models import Course

    students_with_email = [student for student in get_user_model().objects.all() if student.email != '']

    # retrieve courses for last 7 days
    recently_added_courses = Course.objects.all().filter(created_at__gte=timezone.now() - datetime.timedelta(days=7))

    # build string of courses
    courses = ', '.join([str(elem) for elem in recently_added_courses])

    text = f'We have recently added new courses: {courses}. Do not miss!'

    for student in students_with_email:
        send_email_as_admin('New hometask', text, student.email)





