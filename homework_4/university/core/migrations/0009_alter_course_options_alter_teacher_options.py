# Generated by Django 4.0.4 on 2022-06-18 07:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0008_alter_student_options'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='course',
            options={'permissions': (('can_add_course', 'Is user can add course'),)},
        ),
        migrations.AlterModelOptions(
            name='teacher',
            options={'permissions': (('can_edit_teacher', 'Is user can edit teacher'), ('can_add_teacher', 'Is user can add teacher'))},
        ),
    ]
