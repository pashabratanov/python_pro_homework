"""university URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import path, include
from core.views import HomePageView, GenerateStudentView, GenerateLotsStudentsView, GroupsListView, TeachersListView, \
    CreateStudentView, CreateTeacherView, UpdateStudentView, UpdateTeacherView, CoursesListView, SignupView, SigninView,\
    ProfileView, CourseView, LessonView, CreateCourseView, CreateHomeTaskView, CreateLessonView
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', HomePageView.as_view()),
    path('generate-student/', GenerateStudentView.as_view()),
    path('generate-students/', GenerateLotsStudentsView.as_view()),
    path('groups/', GroupsListView.as_view()),
    path('teachers/', TeachersListView.as_view()),
    path('create/student/', CreateStudentView.as_view()),
    path('create/teacher/', CreateTeacherView.as_view()),
    path('update/student/<int:id>/', UpdateStudentView.as_view()),
    path('update/teacher/<int:id>/', UpdateTeacherView.as_view()),
    path('courses/', CoursesListView.as_view()),
    path('signup/', SignupView.as_view()),
    path('signin/', SigninView.as_view()),
    path('logout/', LogoutView.as_view()),
    path('profile/<int:id>/', ProfileView.as_view()),
    path('courses/<int:id>/', CourseView.as_view()),
    path('lessons/<int:id>/', LessonView.as_view()),
    path('create/course/', CreateCourseView.as_view()),
    path('courses/<int:course_id>/create/lesson/', CreateLessonView.as_view()),
    path('courses/<int:course_id>/lessons/<int:lesson_id>/create/hometask/', CreateHomeTaskView.as_view()),

]


if settings.DEBUG == True:
    urlpatterns.append(path('__debug__/', include('debug_toolbar.urls')))
