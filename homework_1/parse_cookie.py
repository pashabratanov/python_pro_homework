def parse_cookie(query: str) -> dict:
    dct: dict = {}

    if ';' in query:
        lst: list = query.split(';')
        lst.remove('')
        for el in lst:
            key, value = el.split('=', 1)
            dct[key] = value
    return dct
