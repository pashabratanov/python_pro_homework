def parse(query: str) -> dict:
    dct: dict = {}
    if '?' not in query:
        return dct

    lst: list = query.split('?')
    if len(lst) > 0:
        inner: list = lst[1].split('&')

        for el in inner:
            if el != '':
                key, value = el.split('=')
                dct[key] = value
    return dct
