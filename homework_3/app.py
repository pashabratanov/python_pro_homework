from flask import Flask, render_template, request, redirect, url_for, abort
from sqlite3 import connect

app = Flask(__name__)
DB_NAME = 'music_lib.sqlite3'


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/tracks/add/', methods=['POST', 'GET'])
def add_track():
    if request.method == 'POST':
        author, name = request.values.get('author'), request.values.get('name')

        if not author or not name:
            context = {'name': name, 'author': author, 'error': 'Name and author are required!!', 'action': 'add'}
            return render_template('track.html', **context)

        with connect(DB_NAME) as conn:
            cursor = conn.cursor()
            cursor.execute("INSERT INTO tracks (author, name) VALUES (?, ?);", (author, name))
            conn.commit()

        return redirect(url_for('tracks'))

    return render_template('track.html', action='add')


@app.route('/tracks/<track_id>/edit/', methods=['POST', 'GET'])
def edit_track(track_id):
    context: dict = {'action': 'edit'}

    if not track_id:
        abort(404)

    if request.method != 'POST':
        with connect(DB_NAME) as conn:
            cursor = conn.cursor()
            cursor.execute("SELECT author, name FROM tracks WHERE id=?;", (track_id,))
            data = cursor.fetchone()
            if not data:
                abort(404)
            context['author'], context['name'] = data
    else:
        author = request.values.get('author')
        name = request.values.get('name')

        if not author or not name:
            context.update({'name': name, 'author': author, 'error': 'Name and author are required!!'})
            return render_template('track.html', **context)

        with connect(DB_NAME) as conn:
            cursor = conn.cursor()
            cursor.execute("UPDATE tracks SET author=?, name=? WHERE id=?;", (author, name, track_id))
            conn.commit()
            return redirect(url_for('tracks'))
    return render_template('track.html', **context)


@app.route('/tracks/<track_id>/delete/')
def delete_track(track_id):
    if not track_id:
        abort(400)

    with connect(DB_NAME) as conn:
        cursor = conn.cursor()
        cursor.execute('DELETE FROM tracks WHERE id=?;', (track_id,))
        conn.commit()

    return redirect(url_for('tracks'))


@app.route('/tracks/')
def tracks():
    base_select_query = 'SELECT id, author, name FROM tracks'

    context = {'tracks': []}

    attr = request.values.get('attr')
    q = request.values.get('q')

    if q and attr in {'name', 'author'}:
        context['q'] = q
        query = base_select_query + f' WHERE {attr} LIKE "%{q}%";'
    else:
        query = base_select_query + ';'

    with connect(DB_NAME) as conn:
        cursor = conn.cursor()
        cursor.execute(query)
        for row in cursor:
            context['tracks'].append({'id': row[0], 'author': row[1], 'name': row[2]})

    return render_template('tracks.html', **context)


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=5000, debug=True)
